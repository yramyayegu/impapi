(function(){	
	var app = angular.module('app');

	app.controller('StatesController', function($scope, $route, $routeParams, $location) {
		$scope.$route = $route;
		$scope.$location = $location;
		$scope.$routeParams = $routeParams;
	});

	app.controller("StatesIndexController", function($scope, $http, components) {
		$scope.predicate = 'id';
		$scope.name = "StatesIndexController";
		$scope.states = [];
		$http.get('//http://api.localhost/cakeadmintest//states/').success(function(data) {
	        $scope.states = data;
			components.paginate($scope, data);
	    });
	});

	app.controller("StatesViewController", function($scope, $http, $routeParams) {
		$scope.name = "StatesViewController";
		$scope.states = [];
		$http.get('//http://api.localhost/cakeadmintest//states/' + $routeParams.id).success(function(data) {
	        $scope.states = data;
	    });
	});

	app.controller("StatesAddController", function($scope, $http, $routeParams, $location) {
		$scope.name = "StatesAddController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest//countries/').success(function(data) {
	        $scope.countries = data;
	    });
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest//states/',
            	method: "POST",
            	data: 'body=' + JSON.stringify({State:$scope.states.State}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.State.type = 'success'){
    				$location.path("/");
    			} else {
    				alert('State could not be added.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.controller("StatesEditController", function($scope, $http, $routeParams, $location) {
		$scope.name = "StatesEditController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest//countries/').success(function(data) {
	        $scope.countries = data;
	    });
		$scope.states = [];
		$http.get('//http://api.localhost/cakeadmintest//states/' + $routeParams.id).success(function(data) {
	        $scope.states = data;
	    });
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest//states/' + $routeParams.id,
            	method: "PUT",
            	data: 'body=' + JSON.stringify({State:$scope.states.State}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.State.type = 'success'){
    				$location.path("/");
    			} else {
    				alert('State could not be updated.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl : '/view/states/index.html',
				controller  : 'StatesIndexController'
			})
			.when('/view/:id', {
				templateUrl : '/view/states/view.html',
				controller  : 'StatesViewController'
			})
			.when('/add', {
				templateUrl : '/view/states/add.html',
				controller  : 'StatesAddController'
			})
			.when('/edit/:id', {
				templateUrl : '/view/states/edit.html',
				controller  : 'StatesEditController'
			})
			.when('/delete/:id', {
		        resolve: {
		            statesDelete: function ($http, $route, $location) {
		               $http.delete('//http://api.localhost/cakeadmintest//states/'+ $route.current.params.id)
						.success(function(data) {
							if(data.State.type = 'success'){
								$location.path("/");
							} else {
								alert('State could not be deleted.')
							}
						}).error(function (data, status, headers, config) {
							alert('State could not be deleted.')
						});
		            }
		       }
			})
	});

})();