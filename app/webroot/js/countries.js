(function(){	
	var app = angular.module('app');

	app.controller('CountriesController', function($scope, $route, $routeParams, $location) {
		$scope.$route = $route;
		$scope.$location = $location;
		$scope.$routeParams = $routeParams;
	});

	app.controller("CountriesIndexController", function($scope, $http, components) {
		$scope.predicate = 'id';
		$scope.name = "CountriesIndexController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest//countries/').success(function(data) {
	        $scope.countries = data;
			components.paginate($scope, data);
	    });
	});

	app.controller("CountriesViewController", function($scope, $http, $routeParams) {
		$scope.name = "CountriesViewController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest//countries/' + $routeParams.id).success(function(data) {
	        $scope.countries = data;
	    });
	});

	app.controller("CountriesAddController", function($scope, $http, $routeParams, $location) {
		$scope.name = "CountriesAddController";
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest//countries/',
            	method: "POST",
            	data: 'body=' + JSON.stringify({Country:$scope.countries.Country}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.Country.type = 'success'){
    				$location.path("/");
    			} else {
    				alert('Country could not be added.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.controller("CountriesEditController", function($scope, $http, $routeParams, $location) {
		$scope.name = "CountriesEditController";
		$scope.countries = [];
		$http.get('//http://api.localhost/cakeadmintest//countries/' + $routeParams.id).success(function(data) {
	        $scope.countries = data;
	    });
		$scope.send = function(){
            $http({
            	url: '//http://api.localhost/cakeadmintest//countries/' + $routeParams.id,
            	method: "PUT",
            	data: 'body=' + JSON.stringify({Country:$scope.countries.Country}),
            	headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    		}).success(function (data, status, headers, config) {
    			if(data.Country.type = 'success'){
    				$location.path("/");
    			} else {
    				alert('Country could not be updated.')
    			}
        	}).error(function (data, status, headers, config) {
	        	alert(status + ' ' + data);
            });
		}
	});

	app.config(function($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl : '/view/countries/index.html',
				controller  : 'CountriesIndexController'
			})
			.when('/view/:id', {
				templateUrl : '/view/countries/view.html',
				controller  : 'CountriesViewController'
			})
			.when('/add', {
				templateUrl : '/view/countries/add.html',
				controller  : 'CountriesAddController'
			})
			.when('/edit/:id', {
				templateUrl : '/view/countries/edit.html',
				controller  : 'CountriesEditController'
			})
			.when('/delete/:id', {
		        resolve: {
		            countriesDelete: function ($http, $route, $location) {
		               $http.delete('//http://api.localhost/cakeadmintest//countries/'+ $route.current.params.id)
						.success(function(data) {
							if(data.Country.type = 'success'){
								$location.path("/");
							} else {
								alert('Country could not be deleted.')
							}
						}).error(function (data, status, headers, config) {
							alert('Country could not be deleted.')
						});
		            }
		       }
			})
	});

})();