<?php

App::uses('ApiController', 'Api.Controller');

class MembersController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function api_1_0_memberSignUp() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->loadModel('User');
            $isUserExisting = $this->User->find('first', array('conditions' => array(
                    'User.user_email' => $requesteddata['user_email'])));
            if (empty($isUserExisting)) {

                if ($this->User->Save($requesteddata)) {
                    $requesteddata['user_id'] = $this->User->id;
                    $requesteddata['email'] = $requesteddata['user_email'];
                    if ($requesteddata['user_role'] === INFLUENCER) {
                        $this->loadModel('Influencer');
                        $this->loadModel('InfluencerCategory');
                        if ($this->Influencer->Save($requesteddata)) {
                            $requesteddata['category_ids'];
                            if (isset($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                                $influencerId = $this->Influencer->id;
                                $category_ids = $requesteddata['category_ids'];
                                $requesteddata['dob'] = '';

                                $categorydata = [];
                                foreach ($category_ids as $infcid) {

                                    $inf['InfluencerCategory']['influencer_id'] = $influencerId;
                                    $inf['InfluencerCategory']['category_id'] = $infcid;
                                    $categorydata[] = $inf;
                                }
                                $this->InfluencerCategory->SaveAll($categorydata);
                                $status = 'SUCCESS';
                                $message = 'Member Registered successfully.';
                                $content = $requesteddata;
                            } else {
                                $template = 'welcome';
                                $subject = 'Welcome to Hashify!';
                                $recipient_name = '';

                                $recipient_name = $requesteddata['first_name'];

                                $recipient_email = $requesteddata['user_email'];
                                $recipient_name = "Hey " . $requesteddata['first_name'];

                                $messaage_body = ' ' . $recipient_name . '<br/><br/>
                            Thanks for joining Hashify Family. We are excited to have you onboard. We are relentlessly working on getting brand sponsorships for you. We will notify you when we find the right campaign for your content. Stay tuned!<br/><br/>

Cheers,<br/>

Team Hashify<br/>

www.hashify.co.';
                                $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);
                                $status = 'SUCCESS';
                                $message = 'Member Registered successfully.';
                                $content = $requesteddata;
                            }
                        } else {
                            $status = 'ERROR';
                            $message = 'Member registration failed';
                            $content = $requesteddata;
                        }
                    } else if ($requesteddata['user_role'] === AGENCY) {
                        $this->loadModel('Agency');
                        if ($this->Agency->Save($requesteddata)) {
                            $status = 'SUCCESS';
                            $message = 'Member Registered successfully.';
                            $content = $requesteddata;
                            $subject = 'Welcome to Hashify!';
                            $recipient_name = '';
                            //   $emailFormat='both';
                            // $viewVars=array(
                            // 'login' => "Login",
                            //            );
                            $recipient_name = "Hi " . $requesteddata['first_name'];
                            //    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                            $recipient_email = $requesteddata['user_email'];
                            //$link ='http://157.230.59.121/web/#/IMP/login';
                            // $login= print "<a href='".$link."'>Hashify</a>"; 

                            $messaage_body = ' ' . $recipient_name . '<br/><br/>
Thanks for signing up with us! We are excited to collaborate with you to create an amazing influencer campaign. You can start planning your campaign with us right away.<br/><br/>
    
We helped brands get millions of impressions through marketing activities with us. Some of the brands who worked with us are <strong>Trivago, Paytm, Tinder, Maybelline, Stayfree</strong> etc.,<br/><br/>

You will soon be assigned an account manager to help you with the campaign creation and understand your requirements better.<br/>

Please write to us at hello@hashify.co for any queries.<br/><br/>

Cheers,<br/>

Team Hashify<br/>

www.hashify.co.';
                            $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);
                        }
                    }
                } else {
                    $status = 'ERROR';
                    $message = 'Something Went Wrong.please try again';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'User Already exits';
                $content = $this->request->data;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_createAdmin() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->loadModel('User');
            $isUserExisting = $this->User->find('first', array('conditions' => array(
                    'User.user_email' => $requesteddata['user_email'])));
            if (empty($isUserExisting)) {
                $requesteddata['user_role'] === ADMIN;
                if ($this->User->Save($requesteddata)) {
                    $requesteddata['user_id'] = $this->User->id;
                    $requesteddata['email'] = $requesteddata['user_email'];
                    $this->loadModel('Admin');
                    if ($this->Admin->Save($requesteddata)) {
                        $status = 'SUCCESS';
                        $message = 'Admin user added successfully.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'Something Went Wrong.please try again';
                        $content = $requesteddata;
                    }
                } else {
                    $status = 'ERROR';
                    $message = 'Something Went Wrong.please try again';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'User Already exits';
                $content = $this->request->data;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_memberSignIn() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            //pr($requesteddata);
            if (isset($requesteddata['email']) && !empty($requesteddata['email']) && isset($requesteddata['password']) && !empty($requesteddata['password'])) {

                $email = $requesteddata['email'];
                $password = $requesteddata['password'];
                $hashedpwd = AuthComponent::password($password);
                // $hashedpwd = Security::encrypt($password,Configure::read('Security.salt'),Configure::read('Security.salt'));
                //  $this->log($hashedpwd,'debug');
                // print_r(Security::decrypt($hashedpwd, Configure::read('Security.salt'), $hmacSalt = null));
                // exit();
                // $this->log($hashedpwd,'debug');
                //print_r($hashedpwd);
                $this->loadModel('User');
                $options = array(
                    'fields' => array(
                        'Agency.id',
                        'Agency.email',
                        'Agency.first_name',
                        'Agency.last_name',
                        'Agency.brand_name',
                        'Agency.gender_id',
                        'Agency.designation',
                        'Agency.phone_no',
                        'Agency.city',
                        'Agency.logo',
                        'Admin.id',
                        'Admin.email',
                        'Admin.first_name',
                        'Admin.last_name',
                        'Admin.gender_id',
                        'Admin.designation',
                        'Admin.phone_no',
                        'Admin.city',
                        'Admin.profile_pic',
                        'Influencer.id',
                        'Influencer.email',
                        'Influencer.first_name',
                        'Influencer.last_name',
                        'Influencer.dob',
                        'Influencer.gender_id',
                        'Influencer.phone_no',
                        'Influencer.city',
                        'Influencer.photo',
                        'Influencer.alternate_number',
                        'Influencer.address',
                        'Influencer.state',
                        'Influencer.country',
                        'User.id',
                        'User.is_active',
                        'User.user_email',
                        'User.user_role',
                    ),
                    'conditions' => array(
                        'User.is_active' => ACTIVE,
                        'User.user_email' => $email,
                        'User.pswd' => $hashedpwd),
                    'recursive' => 0,
                );
                //pr($options);
                $userdata = $this->User->find('first', $options);
                //pr($userdata);                
                if (!$userdata) {
                    //throw new UnauthorizedException('Invalid username or password');
                    $status = 'ERROR';
                    $message = 'Invalid loginid OR password.';
                    $content = $requesteddata;
                } else {

                    //print_r($userdata);
                    $memberid = null;
                    $profile['profile'] = null;
                    if ($userdata['User']['user_role'] === AGENCY) {
                        $memberid = $userdata['Agency']['id'];
                        $profile['profile'] = $userdata['Agency'];
                    } elseif ($userdata['User']['user_role'] === INFLUENCER) {
                        $memberid = $userdata['Influencer']['id'];
                        $profile['profile'] = $userdata['Influencer'];
                    } elseif ($userdata['User']['user_role'] === ADMIN) {
                        $memberid = 0;
                        $profile['profile'] = $userdata['Admin'];
                    }
                    $userid = $userdata['User']['id'];
                    $emailid = $userdata['User']['user_email'];
                    $memberrole = $userdata['User']['user_role'];
                    $token = $this->generateToken($userid, $memberid, $emailid, $memberrole);

                    $status = 'SUCCESS';
                    $message = 'Login success.';
                    $requesteddata['token'] = $token;

                    $profile['profile']['member_id'] = $memberid;
                    $profile['profile']['member_type'] = $memberrole;
                    $profile['profile']['user_id'] = $userid;
                    $profile['profile']['email'] = $emailid;
                    $requesteddata['profile'] = $profile['profile'];
                    //$requesteddata['member_type'] = $memberrole;
                    //$requesteddata['user_id'] = $userid;
                    //$requesteddata['member_id'] = $memberid;
                    $requesteddata['user_type'] = $memberrole;
                    unset($requesteddata['profile']['id']);
                    unset($requesteddata['email']);
                    unset($requesteddata['password']);
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid request.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Bad request.';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_getInfluencers() {
        $this->paginate = array(
            'limit' => 10,
            'fields' => array(
                'id',
                'first_name',
                'phone_no',
                'bio',
            // 'description'
            ),
            'conditions' => array(
                'User.is_active' => 1,
            ),
            'recursive' => 1,
                'order' => array('Influencer.id' => 'desc')
        );
        $this->loadModel('Influencer');
        $influencers = $this->paginate('Influencer');
        $influencers = Set::extract('/Influencer/.', $influencers);
        if ($influencers) {
            $message = 'Records found';
            $status = 'SUCCESS';
            $content = $influencers;
        } else {
            $status = 'SUCCESS';
            $message = 'Records not found';
            $content = $influencers;
        }
        //pr($influencers);
        $pagination = $this->request->params['paging']['Influencer'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));

        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_logout() {

        $jwtsettings = $this->Auth->authenticate['JwtToken.JwtToken'];

        $token = $this->request->header($jwtsettings['header']);

        if ($token) {
            App::import('Vendor', 'JWT' . DS . 'JWT');
            $tokeninfo = JWT\JWT::decode($token, $jwtsettings['pepper'], array('HS256'));

            if ($tokeninfo) {
                $this->loadModel('User');
                $result = $this->User->findByIdAndAuthenticationCode($tokeninfo->user_id, $tokeninfo->authentication_code);
                $this->log($result, 'debug');
                if ($this->User->updateAll(array('authentication_code' => null), array('User.id' => $tokeninfo->user_id))) {
                    $status = "SUCCESS";
                    $message = "Logout success.";
                    $content = null;
                } else {
                    $status = "ERROR";
                    $message = "Logout failed.";
                    $content = null;
                }
            } else {
                $status = "ERROR";
                $message = "Invalid token";
                $content = null;
            }
        } else {
            $status = "ERROR";
            $message = "Invalid token";
            $content = null;
        }

        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_forgotPassword() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->loadModel('User');
            $isMemberExist = $this->User->find('first', array('conditions' => array(
                    //'User.id' => $requesteddata['id'],
                    'User.user_email' => $requesteddata['email'],
                    'User.is_active' => ACTIVE,
            )));
            if (!empty($isMemberExist)) {
                //$newpassword = $this->generateStr(6, 'luds');
                //$this->User->id = $isMemberExist['User']['id'];
                //$userpassword['pswd'] = $newpassword;
                // $hashedpwd=$isMemberExist['User']['pswd'];
                //$password = Security::decrypt($hashedpwd, Configure::read('Security.salt'), Configure::read('Security.salt'));
                $email = $isMemberExist['User']['user_email'];
                $user_id = $isMemberExist['User']['id'];
                $reset_link = "http://www.hashify.co/web/#/IMP/resetpassword?email=$email&id=$user_id";

                //$this->log($hashedpwd,'debug');
                if ($email) {

                    $template = 'welcome';
                    $subject = 'Forgot Password';
                    $recipient_name = '';
                    if ($isMemberExist['User']['user_role'] === ADMIN) {
                        $recipient_name = $isMemberExist['Admin']['first_name'];
                    } else if ($isMemberExist['User']['user_role'] === INFLUENCER) {
                        $recipient_name = $isMemberExist['Influencer']['first_name'];
                    } else if ($isMemberExist['User']['user_role'] === AGENCY) {
                        $recipient_name = $isMemberExist['Agency']['first_name'];
                    }
                    $recipient_email = $isMemberExist['User']['user_email'];

                    $messaage_body = 'Hey there!<br/><br/>Your password reset link is : ' . $reset_link.'<br/><br/>
Cheers,<br/>

Team Hashify<br/>

www.hashify.co.';

                    $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);
                    $status = 'SUCCESS';
                    $message = 'Password Reset Link sent to your email.kindly check your mail';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Member password generation failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Customer';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_resetPassword() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->loadModel('User');
            $isMemberExist = $this->User->find('first', array('conditions' => array(
                    'User.id' => $requesteddata['id'],
                    'User.user_email' => $requesteddata['email'],
            )));
            if (!empty($isMemberExist)) {
                $this->User->id = $isMemberExist['User']['id'];
                $userpassword = $requesteddata['password'];
                if ($this->User->saveField('pswd', $userpassword)) {

                    $template = 'welcome';
                    $subject = 'Change Password';
                    $recipient_name = '';
                    if ($isMemberExist['User']['user_role'] === ADMIN) {
                        $recipient_name = $isMemberExist['Admin']['first_name'];
                    } else if ($isMemberExist['User']['user_role'] === INFLUENCER) {
                        $recipient_name = $isMemberExist['Influencer']['first_name'];
                    } else if ($isMemberExist['User']['user_role'] === AGENCY) {
                        $recipient_name = $isMemberExist['Agency']['first_name'];
                    }
                    $recipient_email = $isMemberExist['User']['user_email'];

                    $messaage_body = 'Hey there!<br/><br/>Your password has been updated.<br/><br/>
Cheers,<br/>

Team Hashify<br/>

www.hashify.co.';

                    $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);

                    $status = 'SUCCESS';
                    $message = 'Member password updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Member password update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid Customer';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_getAgencies() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        }

        $this->loadModel('Agency');
        $this->loadModel('User');
        $this->loadModel('AgencyCategory');
        $this->loadModel('Notification');
        $this->loadModel('Gender');

        $this->User->unbindModel(
                array('hasOne' => array('Agency', 'Influencer', 'Admin'))
        );
        $this->AgencyCategory->unbindModel(
                array('belongsTo' => array('Campaign', 'Notification', 'Agency'))
        );
        $this->Agency->unbindModel(
                array('hasMany' => array('Agency', 'Campaign', 'Notification'), 'belongsTo' => array('User'))
        );
        $this->Gender->unbindModel(
                array('hasMany' => array('CampaignFilter', 'CampaignGender'))
        );
        $this->paginate = array(
            'limit' => 1000,
            //'page' =>$page,
            'fields' => array(
                'id',
                'user_id',
                'email',
                'logo',
                'first_name',
                'last_name',
                'brand_name',
                'designation',
                'gender_id',
                'phone_no',
                'city',
                'created',
                'modified'

            // 'description'
            ),
            'conditions' => array(
            // 'User.is_active' => ACTIVE,
            ),
            'recursive' => 2,
                'order' => array('Agency.id' => 'desc')
        );
        $this->loadModel('Agency');
        $agencies = $this->paginate('Agency');
        $recamp = [];

        if (!empty($agencies)) {
            foreach ($agencies as $agencie) {
                $tmpicms = [];

                $category = Set::classicExtract($agencie['AgencyCategory'], '{n}.Category.category');

                $category2 = implode(",", $category);

                $agencie['Agency']['category'] = $category2;
                $agencie['Agency']['gender'] = $agencie['Gender']['gender'];


                $recamp [] = $agencie;
            }
        }
        $result12 = Set::classicExtract($recamp, '{n}.Agency');

        if ($result12) {
            $message = 'Records found';
            $status = 'SUCCESS';
            $content = $result12;
        } else {
            $status = 'SUCCESS';
            $message = 'Records not found';
            $content = $result12;
        }
        //pr($influencers);
        //$this->set(compact('services',$services));

        $this->set([

            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_updateMembers() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->log($requesteddata, 'debug');
            $this->log($_FILES, 'debug');
            $user_id = $requesteddata['user_id'];

            //$member_id=$requesteddata['member_id'];
            // $member_type=$requesteddata['member_type'];
            $options = array(
                'conditions' => array(
                    'User.is_active' => ACTIVE,
                    'User.id' => $user_id,
                //'User.user_role' => 'AGENCY',
                ), 'recursive' => 0);
            $this->loadModel('User');
            $userdata = $this->User->find('first', $options);


            if (!empty($userdata)) {

                if ($userdata['User']['user_role'] == AGENCY) {

                    $agencie_id = $userdata['Agency']['id'];

                    if (!empty($agencie_id)) {
                        $this->loadModel('Agency');
                        $this->Agency->id = $agencie_id;
                        if (is_uploaded_file($_FILES['logo']['tmp_name'])) {
                            $model = "Agency";
                            $field = "logo";
                            $file = $_FILES['logo'];
                            $requesteddata['logo'] = $this->uploadFile($file, $model, $field);
                        }
                        $this->Agency->set($requesteddata);
                        $this->Agency->save($requesteddata);

                        if (!empty($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                            $this->loadModel('AgencyCategory');
                            $this->AgencyCategory->deleteAll(array('AgencyCategory.agency_id' => $agencie_id), false);
                            $category_ids = $requesteddata['category_ids'];
                            $categorydata = [];
                            foreach ($category_ids as $infcid) {

                                $inf['AgencyCategory']['agency_id'] = $agencie_id;
                                $inf['AgencyCategory']['category_id'] = $infcid;
                                $categorydata[] = $inf;
                            }
                            $this->AgencyCategory->SaveAll($categorydata);
                        }
                        $status = 'SUCCESS';
                        $message = 'agencie updated.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'agencie update failed';
                        $content = $requesteddata;
                    }
                } else if ($userdata['User']['user_role'] == INFLUENCER) {

                    $influencer_id = $userdata['Influencer']['id'];

                    if (!empty($influencer_id)) {
                        $this->loadModel('Influencer');
                        $this->Influencer->id = $influencer_id;
                        if (is_uploaded_file($_FILES['photo']['tmp_name'])) {
                            $model = "Influencer";
                            $field = "photo";
                            $file = $_FILES['photo'];
                            $fileurl = $this->uploadFile($file, $model, $field);
                            // $this->log($fileurl,'debug');
                            $requesteddata['photo'] = $fileurl;
                        }
                        $this->Influencer->set($requesteddata);

                        $this->Influencer->save($requesteddata);

                        //$requesteddata['category_ids'];
                        if (!empty($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                            $this->loadModel('InfluencerCategory');
                            $this->InfluencerCategory->deleteAll(array('InfluencerCategory.influencer_id' => $influencer_id), false);
                            $category_ids = $requesteddata['category_ids'];
                            $categorydata = [];
                            foreach ($category_ids as $infcid) {

                                $inf['InfluencerCategory']['influencer_id'] = $influencer_id;
                                $inf['InfluencerCategory']['category_id'] = $infcid;
                                $categorydata[] = $inf;
                            }
                            $this->InfluencerCategory->SaveAll($categorydata);
                        }
                        // $requesteddata['language_ids'];
                        if (!empty($requesteddata['language_ids']) && is_array($requesteddata['language_ids'])) {
                            $this->loadModel('InfluencerLanguage');
                            $this->InfluencerLanguage->deleteAll(array('InfluencerLanguage.influencer_id' => $influencer_id), false);

                            $language_ids = $requesteddata['language_ids'];
                            $languagedata = [];
                            foreach ($language_ids as $inflid) {

                                $inf['InfluencerLanguage']['influencer_id'] = $influencer_id;
                                $inf['InfluencerLanguage']['language_id'] = $inflid;
                                $languagedata[] = $inf;
                            }
                            $this->InfluencerLanguage->SaveAll($languagedata);
                        }
                        $status = 'SUCCESS';
                        $message = 'influencer updated.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'influencer update failed';
                        $content = $requesteddata;
                    }
                } else if ($userdata['User']['user_role'] == ADMIN) {

                    $admin_id = $userdata['Admin']['id'];

                    if (!empty($admin_id)) {
                        $this->loadModel('Admin');
                        $this->Admin->id = $admin_id;
                        if (is_uploaded_file($_FILES['profile_pic']['tmp_name'])) {
                            $model = "Admin";
                            $field = "profile_pic";
                            $file = $_FILES['profile_pic'];
                            $requesteddata['profile_pic'] = $this->uploadFile($file, $model, $field);
                        }
                        $this->Admin->set($requesteddata);
                        if ($this->Admin->save($requesteddata)) {
                            $status = 'SUCCESS';
                            $message = 'admin updated.';
                            $content = $requesteddata;
                        } else {
                            $status = 'ERROR';
                            $message = 'admin update failed';
                            $content = $requesteddata;
                        }
                    } else {
                        $status = 'ERROR';
                        $message = 'Invalid admin';
                        $content = $requesteddata;
                    }
                } else {
                    $status = 'ERROR';
                    $message = 'Invalid User details';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid User';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = '';
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_getUserProfile() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $user_id = $requesteddata['user_id'];

            $options = array(
                'fields' => array(
                    'Agency.id',
                    'Agency.user_id',
                    'Agency.email',
                    'Agency.first_name',
                    'Agency.last_name',
                    'Agency.brand_name',
                    'Agency.gender_id',
                    'Agency.designation',
                    'Agency.phone_no',
                    'Agency.city',
                    'Agency.logo',
                    'Admin.id',
                    'Admin.user_id',
                    'Admin.email',
                    'Admin.first_name',
                    'Admin.last_name',
                    'Admin.gender_id',
                    'Admin.designation',
                    'Admin.phone_no',
                    'Admin.city',
                    'Admin.profile_pic',
                    'Influencer.id',
                    'Influencer.user_id',
                    'Influencer.email',
                    'Influencer.first_name',
                    'Influencer.last_name',
                    'Influencer.dob',
                    'Influencer.gender_id',
                    'Influencer.phone_no',
                    'Influencer.city',
                    'Influencer.photo',
                    'Influencer.alternate_number',
                    'Influencer.address',
                    'Influencer.state',
                    'Influencer.country',
                    'Influencer.dob',
                    'User.id',
                    'User.is_active',
                    'User.user_email',
                    'User.user_role',
                ),
                'conditions' => array(
                    'User.is_active' => ACTIVE,
                    'User.id' => $user_id,
                //'User.user_role' => 'AGENCY',
                ), 'recursive' => 0);
            $this->loadModel('User');
            $userdata = $this->User->find('first', $options);
            //echo"<pre>";print_r($userdata);"</pre>";exit;

            if (!empty($userdata)) {

                $user_role = $userdata['User']['user_role'];
                if ($user_role == AGENCY) {

                    $options = array(
                        'conditions' => array(
                            'Agency.user_id' => $user_id,
                        ), 'recursive' => 2);
                    $this->loadModel('Agency');
                    $this->loadModel('User');
                    $this->loadModel('AgencyCategory');
                    $this->loadModel('Notification');
                    $this->loadModel('CampaignGender');
                    $this->loadModel('CampaignFilter');
                    $this->loadModel('Gender');

                    $this->User->unbindModel(
                            array('hasOne' => array('Agency', 'Influencer', 'Admin'))
                    );
                    $this->AgencyCategory->unbindModel(
                            array('belongsTo' => array('Campaign', 'Notification', 'Agency'))
                    );
                    $this->Agency->unbindModel(
                            array('hasMany' => array('Agency', 'Campaign', 'Notification'))
                    );
                    $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignGender', 'CampaignFilter'))
                    );

                    $profile = $this->Agency->find('first', $options);
                } elseif ($user_role == INFLUENCER) {

                    $this->loadModel('Influencer');
                    $this->loadModel('InfluencerBankDetail');
                    $this->loadModel('InfluencerCampaign');
                    $this->loadModel('SocialProfile');
                    $this->loadModel('Admin');
                    $this->loadModel('Agency');
                    $this->loadModel('CampaignGender');
                    $this->loadModel('Gender');
                    $this->loadModel('InfluencerCategory');
                    $this->loadModel('CampaignFilter');
                    $this->loadModel('InfluencerLanguage');


                    $options = array(
                        'conditions' => array(
                            'Influencer.user_id' => $user_id,
                        ), 'recursive' => 2);

                    $this->Influencer->unbindModel(
                            array('hasMany' => array('InfluencerBankDetail', 'InfluencerCampaign', 'SocialProfile'))
                    );
                    $this->User->unbindModel(
                            array('hasOne' => array('Influencer', 'Admin', 'Agency'))
                    );
                    $this->Gender->unbindModel(
                            array('hasMany' => array('CampaignGender', 'CampaignFilter'))
                    );
                    $this->InfluencerCategory->unbindModel(
                            array('belongsTo' => array('Influencer'))
                    );
                    $this->InfluencerLanguage->unbindModel(
                            array('belongsTo' => array('Influencer'))
                    );
                    $profile = $this->Influencer->find('first', $options);
                } elseif ($user_role == ADMIN) {

                    $user = Set::extract('/User/.', $userdata);
                    $admin = Set::extract('/Admin/.', $userdata);
                    $gender_id = $admin[0]['gender_id'];
                    $options = array(
                        'fields' => array(
                            'Gender.gender',
                        ),
                        'conditions' => array(
                            'Gender.id' => $gender_id,
                        ), 'recursive' => 0);

                    $this->loadModel('Gender');
                    $gender = $this->Gender->find('first', $options);
                    $profile = Set::merge($user, $admin, $gender);
                }

                if ($profile) {
                    $message = 'userdata found';
                    $status = 'SUCCESS';
                    $content = $profile;
                } else {
                    $status = 'SUCCESS';
                    $message = 'userdata not found';
                    $content = $profile;
                }
            } else {
                $status = 'FAILED';
                $message = 'Invalid User';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $requesteddata;
            ;
        }

        $this->set([
            //  'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_updateProfile() {
        if ($this->request->is('put')) {
            $requesteddata = $this->request->data;
            //$this->log($requesteddata,'debug');
            $user_id = $requesteddata['user_id'];
            // $member_id=$requesteddata['member_id'];
            //  $member_type=$requesteddata['member_type'];
            $options = array(
                'conditions' => array(
                    'User.is_active' => ACTIVE,
                    'User.id' => $user_id,
                //'User.user_role' => 'AGENCY',
                ), 'recursive' => 0);
            $this->loadModel('User');
            $userdata = $this->User->find('first', $options);


            if (!empty($userdata)) {

                if ($userdata['User']['user_role'] == AGENCY) {

                    $agencie_id = $userdata['Agency']['id'];

                    if (!empty($agencie_id)) {
                        $this->loadModel('Agency');
                        $this->Agency->id = $agencie_id;
                        $this->Agency->set($requesteddata);
                        if ($this->Agency->save($requesteddata)) {
                            $requesteddata['category_ids'];
                            if (!empty($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                                $this->loadModel('AgencyCategory');
                                $this->AgencyCategory->deleteAll(array('AgencyCategory.agency_id' => $agencie_id), false);
                                $category_ids = $requesteddata['category_ids'];
                                $categorydata = [];
                                foreach ($category_ids as $infcid) {

                                    $inf['AgencyCategory']['agency_id'] = $this->Agency->id;
                                    $inf['AgencyCategory']['category_id'] = $infcid;
                                    $categorydata[] = $inf;
                                }
                                $this->AgencyCategory->SaveAll($categorydata);
                            }

                            $status = 'SUCCESS';
                            $message = 'agencie updated.';
                            $content = $requesteddata;
                        } else {
                            $status = 'ERROR';
                            $message = 'Agencie not updated';
                            $content = $requesteddata;
                        }
                    } else {
                        $status = 'ERROR';
                        $message = 'Invalid Agencie';
                        $content = $requesteddata;
                    }
                } else if ($userdata['User']['user_role'] == INFLUENCER) {

                    $influencer_id = $userdata['Influencer']['id'];

                    if (!empty($influencer_id)) {
                        $this->loadModel('Influencer');
                        $this->Influencer->id = $influencer_id;
                        $this->Influencer->set($requesteddata);

                        $this->Influencer->save($requesteddata);

                        $requesteddata['category_ids'];
                        if (!empty($requesteddata['category_ids']) && is_array($requesteddata['category_ids'])) {
                            $this->loadModel('InfluencerCategory');
                            $this->InfluencerCategory->deleteAll(array('InfluencerCategory.influencer_id' => $influencer_id), false);
                            $category_ids = $requesteddata['category_ids'];
                            $categorydata = [];
                            foreach ($category_ids as $infcid) {

                                $inf['InfluencerCategory']['influencer_id'] = $influencer_id;
                                $inf['InfluencerCategory']['category_id'] = $infcid;
                                $categorydata[] = $inf;
                            }
                            $this->InfluencerCategory->SaveAll($categorydata);
                        }
                        $requesteddata['language_ids'];
                        if (!empty($requesteddata['language_ids']) && is_array($requesteddata['language_ids'])) {
                            $this->loadModel('InfluencerLanguage');
                            $this->InfluencerLanguage->deleteAll(array('InfluencerLanguage.influencer_id' => $influencer_id), false);
                            $language_ids = $requesteddata['language_ids'];
                            $languagedata = [];
                            foreach ($language_ids as $inflid) {

                                $inf['InfluencerLanguage']['influencer_id'] = $influencer_id;
                                $inf['InfluencerLanguage']['language_id'] = $inflid;
                                $languagedata[] = $inf;
                            }
                            $this->InfluencerLanguage->SaveAll($languagedata);
                        }
                        $status = 'SUCCESS';
                        $message = 'influencer updated.';
                        $content = $requesteddata;
                    } else {
                        $status = 'ERROR';
                        $message = 'influencer update failed';
                        $content = $requesteddata;
                    }
                } else if ($userdata['User']['user_role'] == ADMIN) {

                    $admin_id = $userdata['Admin']['id'];

                    if (!empty($admin_id)) {
                        $this->loadModel('Admin');
                        $this->Admin->id = $admin_id;
                        $this->Admin->set($requesteddata);
                        if ($this->Admin->save($requesteddata)) {
                            $status = 'SUCCESS';
                            $message = 'admin updated.';
                            $content = $requesteddata;
                        } else {
                            $status = 'ERROR';
                            $message = 'admin update failed';
                            $content = $requesteddata;
                        }
                    } else {
                        $status = 'ERROR';
                        $message = 'Invalid admin';
                        $content = $requesteddata;
                    }
                } else {
                    $status = 'ERROR';
                    $message = 'Invalid User details';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid User';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invalid Request';
            $content = $requesteddata;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
