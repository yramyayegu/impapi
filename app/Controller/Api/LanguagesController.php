<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('ApiController', 'Api.Controller');

/**
 * CakePHP LanguagesController.php
 * @author srikanth
 */
class LanguagesController extends ApiController {

public function api_1_0_getLanguages() {
        
        if ($this->request->is('post')) {
                $requesteddata = $this->request->data;
            }
        if ($this->request->is('get')) {
                $requesteddata = $this->request->params['named'];
            }
        
        if(isset($requesteddata['page'])){
            $page = $requesteddata['page'];
        }else{
           $page = 1; 
        }
        if(isset($requesteddata['limit'])){
            $limit = $requesteddata['limit'];
        }else{
            $limit = 10;
        }
        
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
              'fields' => array(
              'id',
              'language',
             // 'description'
                  ), 
            'conditions' => array(
                'Language.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('Language.id' => 'desc')
        );
        $this->loadModel('Language');
        $languages = $this->paginate();
        $languages = Set::extract('/Language/.', $languages);
        if ($languages) {
            $message = 'Languages found';
            $status = 'SUCCESS';
            $content = $languages;
        } else {
            $status = 'SUCCESS';
            $message = 'Languages not found';
            $content = $languages;
        }
        $pagination = $this->request->params['paging']['Language'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
}
