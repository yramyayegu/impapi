<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('ApiController', 'Api.Controller');

/**
 * CakePHP InfluencersController
 * @author srikanth
 */
class InfluencersController extends ApiController {
    
    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getInfluencers
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/influencers/getInfluencers/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Influencers found",
            "content": [
                {
                 "id": "9",
                 "email": "ss@gmail.com",
                 "first_name": "Suresh",
                 "last_name": "S",
                 "dob": "1994-12-03",
                 "gender_id": "1",
                 "phone_no": "1230123012",
                 "city": "Bangalore",
                 "photo": "vd.jpg",
                 "alternate_number": "7456987456",
                 "address": "JublieHills",
                 "state": "Telangana",
                 "country": "India"
                }
     * 
     */
    public function api_1_0_getInfluencers() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        }

        
        $this->loadModel('Influencer');
        $this->loadModel('InfluencerBankDetail');
        $this->loadModel('InfluencerCampaign');
        $this->loadModel('SocialProfile');
        $this->loadModel('Admin');
        $this->loadModel('Agency');
        $this->loadModel('CampaignGender');
        $this->loadModel('Gender');
        $this->loadModel('InfluencerCategory');
        $this->loadModel('CampaignFilter');
        $this->loadModel('InfluencerLanguage');
        $this->loadModel('User');
        $this->Influencer->unbindModel(
                array('hasMany' => array('InfluencerBankDetail', 'InfluencerCampaign', 'SocialProfile'))
        );
        $this->User->unbindModel(
                array('hasOne' => array('Influencer', 'Admin', 'Agency'))
        );
        $this->Gender->unbindModel(
                array('hasMany' => array('CampaignGender', 'CampaignFilter'))
        );
        $this->InfluencerCategory->unbindModel(
                array('belongsTo' => array('Influencer'))
        );
        $this->InfluencerLanguage->unbindModel(
                array('belongsTo' => array('Influencer'))
        );

        $this->paginate = array(
            //'page' => $page,
            'limit' => 1000,
            'conditions' => array(
              //  'User.is_active' => 1,
            ),
            'recursive' => 2,
                'order' => array('Influencer.id' => 'desc')
        );
         
        $this->loadModel('Influencer');
        $influencers = $this->paginate('Influencer');
       // echo"<pre>";print_r($influencers);"</pre>";exit;
        $recamp = [];

        if (!empty($influencers)) {
            foreach ($influencers as $influencer) {
                $tmpicms = [];


                $language = Set::classicExtract($influencer['InfluencerLanguage'], '{n}.Language.language');
                $category = Set::classicExtract($influencer['InfluencerCategory'], '{n}.Category.category');
                // $gender = Set::classicExtract($influencer['Gender'], '{n}.gender');


                $language2 = implode(",", $language);
                $category2 = implode(",", $category);
                // $gender2 = implode(",", $gender);


                $influencer['Influencer']['language'] = $language2;
                $influencer['Influencer']['category'] = $category2;
                $influencer['Influencer']['gender'] = $influencer['Gender']['gender'];
                $influencer['Influencer']['followers'] = $influencer['User']['SocialProfile']['followers'];
                $influencer['Influencer']['posts_count'] = $influencer['User']['SocialProfile']['post_count'];
                $influencer['Influencer']['user_name'] = $influencer['User']['SocialProfile']['user_name'];

                $recamp [] = $influencer;
            }
        }
        $result12 = Set::classicExtract($recamp, '{n}.Influencer');

        // echo"<pre>";print_r($influencers);"</pre>";exit;
        // $influencers = Set::extract('/Influencer/.', $influencers);
        if ($result12) {
            $message = 'influencers found';
            $status = 'SUCCESS';
            $content = $result12;
        } else {
            $status = 'SUCCESS';
            $message = 'influencers not found';
            $content = $result12;
        }
        
        //$this->set(compact('services',$services));
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
