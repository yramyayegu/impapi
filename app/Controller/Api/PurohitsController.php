<?php

App::uses('ApiController', 'Api.Controller');


class PurohitsController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler');
    //public $uses = array('Member');

    public function beforeFilter() {

        parent::beforeFilter();
        $this->Auth->allow(
                'api_1_0_getToken', 'api_1_0_register', 'api_1_0_logout',
                //'api_1_0_getPurohits',
                'api_1_0_meServices', 'api_1_0_meDetails'
        );
    }

    public function api_1_0_getToken() {

        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            pr($requesteddata);
            if (isset($requesteddata['loginid']) && !empty($requesteddata['loginid']) && isset($requesteddata['password']) && !empty($requesteddata['password'])) {

                $mobileno = $requesteddata['loginid'];
                $password = $requesteddata['password'];
                $hashedpwd = AuthComponent::password($password);
                //print_r($hashedpwd);
                $this->loadModel('Member');
                $options = array(
                    'fields' => array(
                        'id',
                        'loginid',
                        'first_name',
                        'last_name',
                        'mobile_number',
                        'addr1',
                        'city',
                        'pin_code',
                        'email'),
                        'conditions' => array(
                        'Member.is_active' => ACTIVE,
                        'Member.loginid' => $mobileno,
                        'Member.password' => $hashedpwd),
                        'recursive' => 0,
                );
                $userdata = $this->Member->find('first', $options);

                if (!$userdata) {
                    //throw new UnauthorizedException('Invalid username or password');
                    $status = 'ERROR';
                    $message = 'Invalid loginid OR password.';
                    $content = $requesteddata;
                } else {

                    //print_r($userdata);

                    $memberid = $userdata['Member']['id'];
                    $loginid = $userdata['Member']['loginid'];
                    $token = $this->generateToken($memberid, $loginid);

                    $status = 'SUCCESS';
                    $message = 'Login success.';
                    $requesteddata['token'] = $token;
                    unset($requesteddata['loginid']);
                    unset($requesteddata['password']);
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invalid request.';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Bad request.';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_getPurohits() {
        //print_r($this->request);

        $this->paginate = array(
            'limit' => 10, /*
              'fields' => array(
              'id',
              'first_name',
              'last_name',
              'mobile_number',
              'addr1',
              'city',
              'pin_code',
              'email'), */
            'conditions' => array(
                'Member.is_active' => ACTIVE,
                'Member.role' => 'PUROHIT'),
            'recursive' => 0,
            'order' => array('Members.id' => 'desc')
        );
        $this->loadModel('Member');
        $customers = $this->paginate();
        if ($customers) {
            $message = 'Records found';
            $status = 'SUCCESS';
            $content = $customers;
        } else {
            $status = 'SUCCESS';
            $message = 'Records not found';
        }
        $pagination = $this->request->params['paging']['Member'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_meServices() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            if ($requesteddata['id']) {
                $this->loadModel('PurohitServiceMaps');
                $customerinfo = $this->PurohitServiceMaps->find('all', array(
                    //'fields' => array('id','customer_firstname','customer_lastname','customer_mobilenumber','customer_addr1','customer_city','customer_pin','email'),
                    'conditions' => array('PurohitServiceMaps.member_id' => $requesteddata['id']),
                    'recursive' => 0
                        )
                );
                //print_r($customerinfo);
                if (!empty($customerinfo)) {
                    $status = 'SUCCESS';
                    $message = 'Purohit Information found';
                    $content = $customerinfo;
                } else {
                    $status = 'ERROR';
                    $message = 'Purohih services not found';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_meDetails() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            if ($requesteddata['id']) {
                $customerinfo = $this->Member->find('first', array(
                    //'fields' => array('id','customer_firstname','customer_lastname','customer_mobilenumber','customer_addr1','customer_city','customer_pin','email'),
                    'conditions' => array('Member.is_active' => ACTIVE, 'Member.id' => $requesteddata['id']),
                    'recursive' => 1
                        )
                );
                //print_r($customerinfo);
                if (!empty($customerinfo)) {
                    $status = 'SUCCESS';
                    $message = 'Purohit Information found';
                    $content = $customerinfo;
                } else {
                    $status = 'ERROR';
                    $message = 'Invaled Customer';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /*
      id
      customer_loginid
      email
     */

    public function api_1_0_forgotPassword() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $isCustomerExist = $this->Customer->find('first', array('conditions' => array(
                    'Customer.id' => $requesteddata['id'],
                    'Customer.email' => $requesteddata['email'],
                    'Customer.customer_loginid' => $requesteddata['customer_loginid'],
            )));
            if (!empty($isCustomerExist)) {
                $newpassword = $this->generateStr(6, 'luds');

                $this->Customer->id = $isCustomerExist['Customer']['id'];
                $cuspassword['customer_password'] = $newpassword;
                if ($this->Customer->save($cuspassword)) {

                    $template = 'welecome';
                    $subject = 'Forgot Password';
                    $recipient_name = $isCustomerExist['Customer']['customer_firstname'];
                    $recipient_email = $isCustomerExist['Customer']['email'];

                    $messaage_body = 'Your new password is : ' . $newpassword;

                    $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);
                    $status = 'SUCCESS';
                    $message = 'Customer updated password sent to your email.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Customer password generation failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Customer';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /*
      id
      customer_loginid
      customer_password
      email
     */

    public function api_1_0_resetPassword() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $isCustomerExist = $this->Customer->find('first', array('conditions' => array(
                    'Customer.id' => $requesteddata['id'],
                    'Customer.customer_loginid' => $requesteddata['customer_loginid'],
            )));
            if (!empty($isCustomerExist)) {
                $this->Customer->id = $isCustomerExist['Customer']['id'];

                if ($this->Customer->save($requesteddata)) {
                    /*
                      $template = 'welecome';
                      $subject = 'Reset Password';
                      $recipient_name = $isCustomerExist['Customer']['customer_firstname'];
                      $recipient_email = $isCustomerExist['Customer']['email'];

                      $messaage_body = 'Your new password updated success fully';

                      $this->sendMail($messaage_body, $subject, $template, $recipient_name, $recipient_email);
                     */
                    $status = 'SUCCESS';
                    $message = 'Customer password updated.';
                    $content = $requesteddata;
                } else {
                    $status = 'ERROR';
                    $message = 'Customer password update failed';
                    $content = $requesteddata;
                }
            } else {
                $status = 'ERROR';
                $message = 'Invaled Customer';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    /*
     *

      {
      location_id:"1",
      email;"",
      customer_firstname: "kiran",
      customer_loginid:"kiran0911",
      customer_mobilenumber:"9246560911",
      customer_addr1:"Hyd",
      customer_city:"",
      customer_pin:"",
      primary_contact_no:""
      }

     */

    public function api_1_0_register() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;
            $this->Customer->create();
            $requesteddata['customer_no'] = CakeTime::convert(time(), Configure::read('Config.timezone'));
            if ($this->Customer->save($requesteddata)) {
                $requesteddata['id'] = $this->Customer->id;
                $status = 'SUCCESS';
                $message = 'Customer register succes.';
                $content = $requesteddata;
            } else {
                $status = 'ERROR';
                $message = 'Invaled Customer Data';
                $content = $requesteddata;
            }
        } else {
            $status = 'ERROR';
            $message = 'Invaled Request';
            $content = $this->request->data;
        }
        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

    public function api_1_0_logout() {
        
        $jwtsettings = $this->Auth->authenticate['JwtToken.JwtToken'];

        $token = $this->request->header($jwtsettings['header']);
        
        if ($token) {
            App::import('Vendor', 'JWT' . DS . 'JWT');
            $tokeninfo = JWT\JWT::decode($token, $jwtsettings['pepper'], array('HS256'));
            
            if ($tokeninfo) {
                $this->loadModel('Member');
                $result = $this->Member->findByIdAndAuthenticationCode($tokeninfo->id, $tokeninfo->authentication_code);
                $this->log($result, 'debug');
                if ($this->Member->updateAll(array('authentication_code' => null), array('Member.id' => $tokeninfo->id))) {
                    $status = "SUCCESS";
                    $message = "Logout success.";
                    $content = null;
                } else {
                    $status = "ERROR";
                    $message = "Logout failed.";
                    $content = null;
                }
            } else {
                $status = "ERROR";
                $message = "Invalid token";
                $content = null;
            }
        } else {
            $status = "ERROR";
            $message = "Invalid token";
            $content = null;
        }

        $this->set([
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }

}
