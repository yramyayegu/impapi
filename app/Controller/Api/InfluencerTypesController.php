<?php

App::uses('ApiController', 'Api.Controller');


class InfluencerTypesController extends ApiController {

    /**
     * Components
     *
     * @var array
     * */
    public $components = array('Paginator', 'RequestHandler',);
    
    public function beforeFilter() {
        parent::beforeFilter();
        
    }
    
    /**
     * @method getAgeGroups
     * 
     * URL: http://localhost:90/impapi/api/1.0/json/influencer_types/getInfluencerTypes/
     * REQUEST :  NULL
     * METHOD : GET or POST    
     * RESPONSE SUCCESS :
     * {
            "status": "SUCCESS",
            "message": "Age groups found",
            "content": [
                {
                    "id": "14",
                    "influencer_type": "Celebrities"
                },
                {
                    "id": "13",
                    "influencer_type": "Digital Stars"
                }
     *          ...
     *          ...
     * 
            ],
            "pagination": {
                "page": 1,
                "current": 10,
                "count": 14,
                "prevPage": false,
                "nextPage": true,
                "pageCount": 2,
                "limit": 10,
                "paramType": "named"
            }
        }
     * 
     */
    
    public function api_1_0_getInfluencerTypes() {
        if ($this->request->is('post')) {
            $requesteddata = $this->request->data;                
        }
        if ($this->request->is('get')) {
            $requesteddata = $this->request->params['named'];
        } 
        
        if(isset($requesteddata['page'])){
            $page = $requesteddata['page'];
        }else{
           $page = 1; 
        }
        if(isset($requesteddata['limit'])){
            $limit = $requesteddata['limit'];
        }else{
            $limit = 10;
        }
            
        $this->paginate = array(
            'page' => $page,
            'limit' => $limit, 
              'fields' => array(
              'id',
              'influencer_type',
                  ), 
            'conditions' => array(
                'InfluencerType.is_active' => ACTIVE,
                ),
            'recursive' => -1,
            'order' => array('InfluencerType.id' => 'desc')
        );
        $this->loadModel('InfluencerType');
        $influencertypes = $this->paginate();
        $influencertypes = Set::extract('/InfluencerType/.', $influencertypes);
        if ($influencertypes) {
            $message = 'Influencer types found';
            $status = 'SUCCESS';
            $content = $influencertypes;
        } else {
            $status = 'SUCCESS';
            $message = 'Influencer types not found';
            $content = $influencertypes;
        }
        $pagination = $this->request->params['paging']['InfluencerType'];
        unset($pagination['order']);
        unset($pagination['options']);
        //$this->set(compact('services',$services));
        $this->set([
            'pagination' => $pagination,
            'status' => $status,
            'message' => $message,
            'content' => $content,
            '_serialize' => ['status', 'message', 'content', 'pagination']
        ]);
        $this->render('/' . $this->request->params['ext']);
    }
    
  
}
