<?php

/*
 * 
 *   API Routes with versions...
 */        


App::uses('ApiRoute', 'Api.Routing/Route');

Router::connect('/api/:version/:ext/:controller/:action/*',
    array(
        //'plugin' => 'Api',
        'prefix' => 'api',
        'version' => ':version',
        'ext' => ':ext'
    ),array( 'routeClass' => 'ApiRoute','version' => '1\.0|1.1\2\.0|2\.1')
);

Router::parseExtensions('xml','json');