<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Campaign $Campaign
 */
class CampaignAgeGroup extends AppModel {

   
    
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'AgeGroup' => array(
            'className' => 'AgeGroup',
            'foreignKey' => 'age_group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
