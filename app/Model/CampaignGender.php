<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Campaign $Campaign
 */
class CampaignGender extends AppModel {

    
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Gender' => array(
            'className' => 'Gender',
            'foreignKey' => 'gender_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
