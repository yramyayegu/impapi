<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Campaign $Campaign
 */
class CampaignDeliverable extends AppModel {

   
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Deliverable' => array(
            'className' => 'Deliverable',
            'foreignKey' => 'deliverable_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
