<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP InfluencerLanguage
 * @author srikanth
 */
class InfluencerLanguage extends AppModel {
    
    public $belongsTo = array(
		'Influencer' => array(
			'className' => 'Influencer',
			'foreignKey' => 'influencer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'language_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
}
