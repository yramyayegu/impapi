<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Category $Category
 */
class CampaignCategory extends AppModel {

   
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
