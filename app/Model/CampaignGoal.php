<?php

App::uses('AppModel', 'Model');

/**
 * DocType Model
 *
 * @property Goal $Goal
 */
class CampaignGoal extends AppModel {

  
    public $belongsTo = array(
        'Campaign' => array(
            'className' => 'Campaign',
            'foreignKey' => 'campaign_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Goal' => array(
            'className' => 'Goal',
            'foreignKey' => 'goal_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
