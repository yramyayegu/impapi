<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 */
class Category extends AppModel {

    public $actsAs = array('Sluggable.Sluggable' => array(
            'label' => 'category',
            'slug' => 'category_code',
            'lowercase' => false, // Do we lowercase the slug ?
            'uppercase' => true, // Do we uppercase the slug ?
            'separator' => '-',
            'length' => 350,
            'overwrite' => true
    ));

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'category' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Category can\'t be blank',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('isUniqueCategory'),
                'message' => 'This category is already exist.'
            ),
        ),
        'category_code' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Category code can\'t be blank',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'is_active' => array(
            'boolean' => array(
                'rule' => array('boolean'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * Before isUniqueCategory
     * @param array $options
     * @return boolean
     */
    function isUniqueCategory($check) {

        $category = $this->find(
                'first', array(
            'fields' => array(
                'Category.id',
                'Category.category'
            ),
            'conditions' => array(
                'Category.category' => $check['category']
            )
                )
        );

        if (!empty($category)) {
            if ($this->data[$this->alias]['category'] != $category['Category']['category']) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

}
