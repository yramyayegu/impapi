<?php

App::uses('AppModel', 'Model');

/**
 * AgeGroup Model
 *
 * @property CampaignAgeGroup $CampaignAgeGroup
 */
class AgeGroup extends AppModel {


    public $hasMany = array(
        'CampaignFilter' => array(
            'className' => 'CampaignFilter',
            'foreignKey' => 'age_groups',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CampaignAgeGroup' => array(
            'className' => 'CampaignAgeGroup',
            'foreignKey' => 'age_group_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
