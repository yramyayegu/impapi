<?php

App::uses('AppModel', 'Model');

/**
 * InfluencerType Model
 *
 * @property CampaignFilter $CampaignFilter
 */
class InfluencerType extends AppModel {

   
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CampaignFilter' => array(
            'className' => 'CampaignFilter',
            'foreignKey' => 'influencer_types',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CampaignInfluencerType' => array(
            'className' => 'CampaignInfluencerType',
            'foreignKey' => 'influencer_type_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
