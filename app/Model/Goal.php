<?php

App::uses('AppModel', 'Model');

/**
 * Goal Model
 *
 * @property CampaignFilter $CampaignFilter
 */
class Goal extends AppModel {

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CampaignFilter' => array(
            'className' => 'CampaignFilter',
            'foreignKey' => 'goals',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CampaignGoal' => array(
            'className' => 'CampaignGoal',
            'foreignKey' => 'goal_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
