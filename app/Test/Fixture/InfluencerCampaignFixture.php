<?php
/**
 * InfluencerCampaign Fixture
 */
class InfluencerCampaignFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'influencer_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'campaign_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'is_read' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'campaign_status_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'read_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'status_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'reject_reason' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'influencer_id' => '',
			'campaign_id' => '',
			'is_read' => 1,
			'campaign_status_id' => 1,
			'created' => '2018-12-04 06:33:13',
			'modified' => '2018-12-04 06:33:13',
			'read_date_time' => '2018-12-04 06:33:13',
			'status_date_time' => '2018-12-04 06:33:13',
			'reject_reason' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);

}
