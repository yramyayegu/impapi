<?php
/**
 * Notification Fixture
 */
class NotificationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'agency_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'campaign_id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'unsigned' => false),
		'notification_status_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'notification_msg' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'notification_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '',
			'agency_id' => '',
			'campaign_id' => '',
			'notification_status_id' => 1,
			'notification_msg' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'notification_type_id' => 1,
			'created' => '2018-12-04 06:33:14',
			'modified' => '2018-12-04 06:33:14'
		),
	);

}
